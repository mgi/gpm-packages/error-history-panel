Provides the user with an overview of the errors the system has encountered.

### MGI Error Reporter Framework

The MGI Error Reporter Framework is a high level tool to help catch and report errors. It is designed to be extremely modular and flexible, so it can be tweaked for your application.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
